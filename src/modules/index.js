import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import counter from './counter';
import featured from './featured';

export default combineReducers({
  router: routerReducer,
  counter,
  featured,
});
