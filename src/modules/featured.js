export const GET_FEATURED = 'featured/GET_FEATURED';

const initialState = {
  items: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_FEATURED:
      return {
        ...state,
        items: action.payload,
      }
    default:
      return { ...state }
  }
}

export const getFeatured = () => {
  let res = [{ imgPath: "http://via.placeholder.com/450x600", title: "Test1", url: "/" },{ imgPath: "http://via.placeholder.com/450x600", title: "Test2", url: "/" },{ imgPath: "http://via.placeholder.com/450x600", title: "Test3", url: "/" },{ imgPath: "http://via.placeholder.com/450x600", title: "Test4", url: "/" },{ imgPath: "http://via.placeholder.com/450x600", title: "Test5", url: "/" },{ imgPath: "http://via.placeholder.com/450x600", title: "Test6", url: "/" }]
  return dispatch => {
    dispatch({
      type: GET_FEATURED,
      payload: res
    })
  }
}