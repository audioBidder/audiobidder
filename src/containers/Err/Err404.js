import React, { Component } from 'react';

export default class Err404 extends Component {
  render() {
    return (
      <div className="container text-center" style={{marginTop: 200}}>
        <h3>Error 404 page not found. </h3>
        <h3>If you believe there should be a page here contact error404@audiobidder.com and quote the address you tried to access</h3>
      </div>
    )
  }
}