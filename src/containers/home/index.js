import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import HeroVideo from '../../partials/heroVideo';
import SummaryCentered from '../../partials/summaryCentered';
import Featured from '../../partials/featured';

import {
  getFeatured
} from '../../modules/featured';

import './index.css';

const Home = props => (
  <div className="homeWrapper">
  trjkgrhjgrehugiyvuehrjghiuregybuhyhibyuvghbihugyuvg
    <HeroVideo url={'/assets/videos/Ma-Vibes'}/>
    <SummaryCentered heading={"What We Do?"} summary={<div><p>Short loin pork ut sirloin ut kielbasa in aliqua laboris salami officia. Labore shankle pork belly capicola velit. Tongue cupidatat magna turkey bresaola short ribs. Tongue aliquip t-bone fatback commodo kevin. Pariatur ut picanha kevin, jowl spare ribs biltong pastrami.</p></div>} />
    <div className="container">
      <Featured items={props.items} />
    </div>
  </div>
);

const mapStateToProps = state => ({
  items: state.featured.items,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getFeatured,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Home);