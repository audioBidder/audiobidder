import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Example from '../example';
import Home from '../home';
import Nav from '../Navbar';
import Err404 from '../Err/Err404';

import {
  getFeatured
} from '../../modules/featured';


import './index.css';

const App = props => (
  <div>
    <header>
      <Nav />
    </header>

    <main>
      <Switch>
        <Route exact path="/ex" component={Example} />
        <Route exact path='/' component={Home} onEnter={props.getFeatured} />
        <Route component={Err404} />
      </Switch>
    </main>
  </div>
);

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getFeatured,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(App);
