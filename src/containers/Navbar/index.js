import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import './index.css';

const Navbar = props => (
  <nav className="navbar navbar-expand-sm navbar-custom">
    <div className="container">
    <a className="navbar-brand" href="/"><img src="/assets/images/logo.png" alt="logo" /></a>
    <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>

    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav ml-auto">
        <li className="nav-item active">
          <a className="nav-link" href="/">Home <span className="sr-only">(current)</span></a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="/about">About</a>
        </li>
        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Job Types
          </a>
          <div className="dropdown-menu" aria-labelledby="navbarDropdown">
            <Link to='/type/singer' className="dropdown-item" >Singer</Link>
            <Link to='/type/mixer' className="dropdown-item" >Mixer</Link>
            <Link to='/type/writer' className="dropdown-item" >Writer</Link>
            <Link to='/type/musician' className="dropdown-item" >Musician</Link>
            <Link to='/type/coach' className="dropdown-item" >Vocal Coach</Link>
          </div>
        </li>
      </ul>
    </div>
    </div>
  </nav>
);

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => bindActionCreators({},dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Navbar);