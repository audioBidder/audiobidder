import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import './index.css';

const heroVideo = props => (

<div className="homepage-hero-module">
    <div className="video-container">
        <div className="filter">
            <p className="title">TEST TITLE TEXT</p>
        </div>
        <video autoPlay loop className="fillWidth">
            <source src={`${props.url}.mp4`} type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
        </video>
        <div className="poster hidden">
            <img src={`${props.url}.jpg`} alt="" />
        </div>
    </div>
</div>

);

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(heroVideo);