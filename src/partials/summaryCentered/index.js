import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import './index.css';

const summaryCentered = props => (
  <div className="summaryCenteredWrapper container">
    <div className="text-center">
      <div className="title">
        <h3>{props.heading}</h3>
      </div>
      <div className="summary">
        {props.summary}
      </div>
    </div>
  </div>
);

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(summaryCentered);