import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getFeatured } from '../../modules/featured';

import './index.css';

class Featured extends React.Component {

  componentWillMount() {
    this.props.getFeatured();
  }

  render(){
    return (
      <div className="row featuredWrapper text-center">
        {
          this.props.items.length === 0 ? 

            ""

          :

          this.props.items.map(function(item, index) {
            return (
              <a href={item.url} className="col-12 col-sm-6 col-md-4" key={index}>
                <img src={item.imgPath} className="img-fluid" alt="" />
                <h5>{item.title}</h5>
              </a>
            )
          })
        }
      </div>
    )
  }
}

function mapStateToProps(state) {
  return { items: state.featured.items }
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getFeatured: getFeatured }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Featured);